public class Ticket {
    private String penumpang;
    private String asal;
    private String tujuan;
    
    //method untuk mengambil nama penumpang
    public String getNamaPenumpang(){
        return penumpang;
    }
    
    //method untuk mengambil asal penumpang
    public String getAsal(){
        return asal;
    }
    
    //method untuk mengambil tujuan penumpang
    public String getTujuan(){
        return tujuan;
    }
    
    //Construtor Overloading digunakan pada Constructor Ticket
    //Contstructor komuter
    public Ticket(String penumpang){
         this.penumpang = penumpang;
    }
    //Constructor KAJJ
    public Ticket(String penumpang, String asal, String tujuan){
        this.penumpang = penumpang;
        this.asal = asal;
        this.tujuan = tujuan;
    }
}