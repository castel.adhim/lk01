public class Kereta {
    private String namaKereta;
    private Ticket[] daftarTiket;
    private int jumlahTiket;
    private int jumlahPenumpang;
    
    //Constructor untuk mengambil nama penumpang
    public Kereta() {
        namaKereta = "Komuter";
        jumlahTiket = 1000;//jumlah tiket tiket yang ada
        daftarTiket = new Ticket[jumlahTiket];//membuat array untuk kereta Komuter
    }
    
    //Constructor untuk kereta KAJJ
    public Kereta(String namaKereta, int jumlahTiket) {
        this.namaKereta = namaKereta;
        this.jumlahTiket = jumlahTiket;
        daftarTiket = new Ticket[this.jumlahTiket];//membuat array untuk kereta KAJJ
    }
    
    //Method Overloading terjadi pada method tambah tiket yang penentunya ada pada parameternya
    //method digunakan untuk menambah tiket komuter dengan menggunakan parameter penumpang
    public void tambahTiket(String penumpang) {
        if (jumlahPenumpang < jumlahTiket) {
            Ticket ticket = new Ticket(penumpang);
            daftarTiket[jumlahPenumpang] = ticket;
            jumlahPenumpang++;//untuk menambah jumlah penumpang sebagai tanda berapa tiket yang telah berhasil dipesan
            System.out.println("========================================================");
            if ((jumlahTiket-jumlahPenumpang)>=30){
                System.out.println("Tiket berhasil dipesan");}
            //jika tiket kurang dari 30, akan diberitahu sisa tiket yang masih ada
            else if((jumlahTiket-jumlahPenumpang)<30){
                System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia: "+(jumlahTiket-jumlahPenumpang));}
        } 
        else{System.out.println("========================================================");
            System.out.println("Kereta telah habis dipesan, Silahkan cari jadwal keberangkatan lainnya");
        }
    }
    
    //method digunakan untuk menambah tiket kereta KAJJ dengan parametetr nama penumpang, asal, dan tujuan.
    public void tambahTiket(String penumpang, String asal, String tujuan) {
        if (jumlahPenumpang < jumlahTiket) {
            Ticket ticket = new Ticket(penumpang, asal, tujuan);
            daftarTiket[jumlahPenumpang] = ticket;
            jumlahPenumpang++;
            System.out.println("========================================================");
            //apabila tiket tersisa 30 atau dibawahnya 
            //sistem akan menampilkan tiket tersedia
            if ((jumlahTiket - jumlahPenumpang) >= 30) {
                System.out.println("Tiket berhasil dipesan");
            } else if ((jumlahTiket - jumlahPenumpang) < 30) {
                System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia: " + (jumlahTiket - jumlahPenumpang));
            }
        }
        else {//pemberitahuan apabila tiket kereta habis
            System.out.println("========================================================");
            System.out.println("Kereta telah habis dipesan, Silahkan cari jadwal keberangkatan lainnya");
        }
    } 
    
    public void tampilkanTiket(){//method unutk menampilkan hasil tiket
        System.out.println("========================================================");
        System.out.println("Tiket untuk kereta api " + namaKereta + ":");
        System.out.println("-----------------------------");
        for (int a = 0; a < jumlahPenumpang; a++) {
            System.out.println("Nama\t: " + daftarTiket[a].getNamaPenumpang());
            //kode getAsal() adalah penentu apakah kereta ini komuter atau kereta kAJJ.
            //jika terdeteki memiliki asal maka termasuk ke tike kereta KAJJ
            if (daftarTiket[a].getAsal()!= null) {
            System.out.println("Asal\t: " + daftarTiket[a].getAsal());
            System.out.println("Tujuan\t: " + daftarTiket[a].getTujuan());
            System.out.println("-----------------------------");}
        }
    }

    
}